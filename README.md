# Solutions to HackerRank Functional Programming Challenges

This repository contains solutions to the HackerRank Functional Programming Challenges, available at [https://www.hackerrank.com/categories/fp][1]. These solutions are provided "as is". I give no guarantees that they will work as expected. Please refrain from using my solutions in the site.

Due to HackerRank's requirements of enclosing all Scala solutions in an object named `Solution` and adding the fact that in some problems only a function is be submitted, there is currently no automatic and consistent way of compiling and testing locally the solutions of this repository. If you want to try them, please load them in each problem's online editor.

## Problems solved

Here is a list of the problems currently in this repository. Problems marked with ✓ are done, while problems with ✗ are not complete and/or don't comply with performance requirements.

### Introduction

* ✓ Solve me first FP (`fp-solve-me-first.scala`)
* ✓ Update List (`fp-update-list.scala`)
* ✓ List Replication (`fp-list-replication.scala`)
* ✓ Filter positions in a list (`fp-filter-positions-in-a-list.scala`)
* ✓ Filter Array (`fp-filter-array.scala`)
* ✓ Sum of odd elements (`fp-sum-of-odd-elements.scala`)
* ✓ Array Of N Elements (`fp-array-of-n-elements.scala`)
* ✓ List Length (`fp-list-length.scala`)
* ✓ Reverse a list (`fp-reverse-a-list.scala`)
* ✓ Hello World N Times (`fp-hello-world-n-times.scala`)
* ✓ Hello World (`fp-hello-world.scala`)
* ✓ Evaluating e^x (`eval-ex.scala`)
* ✓ Area Under Curves and Volume of Revolving a Curve (`area-under-curves-and-volume-of-revolving-a-curv.scala`)

### Recursion

* ✓ Functional Programming: Warmups in Recursion - Fibonacci Numbers (`functional-programming-warmups-in-recursion---fibonacci-numbers.scala`)
* ✓ Pascal's Triangle (`pascals-triangle.scala`)

[1]: https://www.hackerrank.com/categories/fp
